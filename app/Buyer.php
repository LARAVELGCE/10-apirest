<?php

namespace App;

/**
 * App\Buyer
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Transaction[] $transactions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Buyer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Buyer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Buyer query()
 * @mixin \Eloquent
 */
class Buyer extends User
{
    /* --- Relacion 1 --  1-M---
    cada buyer tiene muchastransactions */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
