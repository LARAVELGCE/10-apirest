<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
         return response()->json(['data' => $users],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationCode();
        $data['admin'] = User::REGULAR_USER;

        $user = User::create($data);

         return response()->json(['data' => $user],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userUnico = User::findOrFail($id);
         return response()->json(['data' => $userUnico],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $rules = [
            // excepto del email que tiene
            'email' => 'email|unique:users,email,' . $user->id,
            //
            'password' => 'min:6|confirmed',
            // veridicar que el valor de admin este en uno de ellos
            'admin' => 'in:' . User::ADMIN_USER . ',' . User::REGULAR_USER,
        ];
        $this->validate($request, $rules);
        // si es que tiene un nombre en el input
        if ($request->has('name')) {
            // actualizamos el nuevo nombre que le vamos a poner
            $user->name = $request->name;
        }

        // Si es que tiene un email y ademas si es diferente al que le vamos
        // a mandar
        if ($request->has('email') && $user->email != $request->email) {
            $user->verified = User::UNVERIFIED_USER;
            $user->verification_token = User::generateVerificationCode();
            // Guarda el email
            $user->email = $request->email;
        }
        // si es que tiene password
        if ($request->has('password')) {
            // actualizamos el password
            $user->password = bcrypt($request->password);
        }
        if ($request->has('admin')) {
            if (!$user->isVerified()) {
                return response()->json(['error' => 'Unicamente los usuarios verificados pueden cambiar su valor
                de administrador', 'code' => 409], 409);
            }
            $user->admin = $request->admin;
        }
        // si el usuario no envio nada
        //
        if (!$user->isDirty()) {
            return response()->json(['error' => 'Se debe de especificar al menos un valor diferente para actualizar', 'code' => 422], 422);
        }
        $user->save();
        return response()->json(['data' => $user], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
