<?php

namespace App;

/**
 * App\Seller
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seller newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seller newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seller query()
 * @mixin \Eloquent
 */
class Seller extends User
{
    /* ---  Relacion 3 --  1-M --- */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
