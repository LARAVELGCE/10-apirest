<?php

use Faker\Generator as Faker;
use App\Seller;
use App\User;

$factory->define(App\Transaction::class, function (Faker $faker) {
    // obtener todos los vendedores que tengan un producto para que sea vendedor
    $seller = Seller::has('products')->get()->random();
    // Puede ser un usuario cualquiera excepto
    $buyer = User::all()->except($seller->id)->random();
    return [
        'quantity' => $faker->numberBetween(1, 3),
        'buyer_id' => $buyer->id,
        'product_id' => $seller->products->random()->id,
        // User::inRandomOrder()->first()->id
    ];
});
