<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Product;
use App\Category;
use App\Transaction;
//use DB;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS = 0');
       // DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      //  DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::disableForeignKeyConstraints();

        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();

        Schema::enableForeignKeyConstraints();


       // DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $usersQuantity = 1000;
        $categoriesQuantity = 30;
        $productsQuantity = 1000;
        $transactionsQuantity = 1000;
        factory(User::class, $usersQuantity)->create();
        factory(Category::class, $categoriesQuantity)->create();
        // Para cada producto se elige aleatoriamente de 1 a 5 categorias
        factory(Product::class, $productsQuantity)->create()->each(
            function ($product) {
                $categories = Category::all()->random(mt_rand(1, 5))->pluck('id');
                $product->categories()->attach($categories);
            });
        /*
        Ca  Pr
        2	1
        3	1
        15	1
        16	1
        8	2
        4	3
        10	3
        29	3
        30	3
        */

        factory(Transaction::class, $transactionsQuantity)->create();
    }
}
